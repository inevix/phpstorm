# phpstorm

How to reset a PHPStorm trial period

## Windows:

Go `C:\Users\username\.PhpStorm*`<br/>
Remove folder `config\eval`<br/>
Remove file `config\options\other.xml`<br/>
Press hotkeys `win+r`, type `regedit`<br/>
Go `HKEY_CURRENT_USER\Software\JavaSoft\Prefs`<br/>
Remove row `/Jet/Brains./User/Id/On/Machine`<br/>
Remove under folder `jetbrains`<br/>
Run PHPStorm without rebooting


## Linux:

`rm -rf ~/.PhpStorm*/config/eval`<br/>
`rm -rf ~/.PhpStorm*/config/options/other.xml`<br/>
`rm -rf ~/.java/.userPrefs/jetbrains/phpstorm`

## MacOS:

`rm -rf $HOME/Library/Preferences/PhpStorm*/eval/`<br/>
`rm -rf $HOME/Library/Preferences/PhpStorm*/options/other.xml`<br/>
`rm -rf $HOME/Library/Preferences/jetbrains.phpstorm.*.plist/`<br/>


## Important

If you use old version, then remove file `config/options/options.xml` instead of `other.xml`